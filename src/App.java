import dao.AgenceDAO;
import dao.CompteDAO;
import dao.IDAO;
import models.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class App {
    private static Agence agence = new Agence( 1, "ANGERS49000", "Rue des Lices" );
    private static Scanner sc = new Scanner( System.in );
    private static final String BACKUPS_DIR = "./resources/backups/";
    private static final IDAO<Integer, Compte> dao = new CompteDAO();

    public static void main(String[] args) {
       dspMainMenu();
    }

    public static void dspMainMenu() {

        try {
            new AgenceDAO().create(agence);
        } catch (SQLIntegrityConstraintViolationException e){
            System.out.println("L'agence existe déjà en base");
            try {
                agence = new AgenceDAO().findByCode(agence.getCode());
                System.out.println("Agence récupérée avec succès");
            } catch (SQLException | IOException | ClassNotFoundException ex) {
                System.err.println("Impossible de récupérer l'agence");
            }
        }
        catch (SQLException | IOException | ClassNotFoundException e) {
            System.err.println("Impossible de créer l'agence en base");
        }
        //TODO sélection manuelle d'une agence pour la session

        try {
            agence.setComptes(dao.findByAgence(agence.getId()));
        } catch (SQLException | IOException | ClassNotFoundException e) {
            System.out.println("Impossible de récupérer les comptes");
        }

        int response;
        boolean first = true;
        do {
            if ( !first ) {
                System.out.println( "Mauvais choix... merci de recommencer !" );
            }
            System.out.println( "========================================" );
            System.out.printf(  "====== GESTION COMPTES %s ====== %n", agence
            .getCode());
            System.out.println( "========================================" );
            System.out.println( "1 - Ajouter un nouveau compte" );
            System.out.println( "2 - Modifier un compte existant" );
            System.out.println( "3 - Supprimer un compte existant" );
            System.out.println( "4 - Lister les comptes" );
            System.out.println( "5 - Quitter" );
            System.out.print( "Entrez votre choix : " );
            try {
                response = sc.nextInt();
            } catch ( InputMismatchException e ) {
                response = -1;
            } finally {
                sc.nextLine();
            }
            first = false;
        } while ( response < 1 || response > 5 );

        switch ( response ) {
            case 1:
                ajouterCompte();
                break;
            case 2:
                modifierCompte();
                break;
            case 3:
                supprimerCompte();
                break;
            case 4:
                listeComptes();
                break;
        }
    }

    private static void ajouterCompte(){
        System.out.println( "======================================" );
        System.out.println( "======== CREATION D'UN COMPTE ========" );
        System.out.println( "======================================" );

        int choix;
        Compte compte = null;
        double solde;
        double decouvert;
        float tauxInteret;
        boolean first = true;

        do {
            if ( !first ) {
                System.out.println( "Mauvais choix... merci de recommencer !" );
            }

            System.out.println("Choisissez un type de compte :");
            System.out.println("1. Simple");
            System.out.println("2. Epargne");
            System.out.println("3. Payant");
            
            try {
                choix = sc.nextInt();
            } catch ( InputMismatchException e ) {
                choix = -1;
            } finally {
                sc.nextLine();
            }
            first = false;
        } while ( choix < 1 || choix > 3 );

        do{
            System.out.print("Solde :");
            try{
                solde = sc.nextDouble();
            }catch (InputMismatchException e){
                solde = -1;
                System.out.println("Erreur de saisie, recommencez");
            }
            finally {
                sc.nextLine();
            }
        } while (solde < 0);

        switch (choix) {
            case 1:
                do{
                    System.out.print("Découvert autorisé :");
                    try {
                        decouvert = sc.nextDouble();
                    }catch (InputMismatchException e){
                        decouvert = -1;
                        System.out.println("Erreur de saisie, recommencez");
                    }
                    finally {
                        sc.nextLine();
                    }
                }while (decouvert < 0);

                compte = agence.creerCompteSimple(solde, decouvert);
                break;
            case 2:
                do{
                    System.out.print("Taux interêt :");
                    try {
                        tauxInteret = sc.nextFloat();
                    }catch (InputMismatchException e){
                        tauxInteret = -1;
                        System.out.println("Erreur de saisie, recommencez");
                    }
                    finally {
                        sc.nextLine();
                    }
                }while (tauxInteret < 0);

                compte = agence.creerCompteEpargne(solde, tauxInteret);
                break;
            case 3:
                compte = agence.creerComptePayant(solde);
                break;
        }

        try{
            dao.create(compte);
            System.out.println("Compte créé avec succès");
            sc.nextLine();
        }catch ( IOException | ClassNotFoundException | SQLException e ) {
            System.err.println( e.getMessage() );
        }

        createCSV(compte);

        dspMainMenu();
    }

    private static void modifierCompte() {
        System.out.println( "======================================" );
        System.out.println( "========= MODIFIER UN COMPTE =========" );
        System.out.println( "======================================" );

        for (Compte compte : agence.getComptes()){
            System.out.printf("%s : %s", compte.getId(), compte);
        }

        Compte compte = null;
        int compteID;

        do{
            System.out.print("Choisissez le compte à modifier :");

            try {
                compteID = sc.nextInt();
            } catch ( InputMismatchException e ) {
                compteID = -1;
            } finally {
                sc.nextLine();
            }

            try{
                compte = dao.findById(compteID);
            }catch ( IOException | ClassNotFoundException | SQLException e ) {
                System.err.println( e.getMessage() );
            }
        }while (compte == null || compteID < 0);

        int choix;
        boolean first = true;
        double soldeAvantAction = compte.getSolde();
        double montant;

        do {
            if ( !first ) {
                System.out.println( "Mauvais choix... merci de recommencer !" );
            }

            System.out.println("Que souhaitez vous faire ? ");
            System.out.println("1. Ajouter de l'argent");
            System.out.println("2. Retirer de l'argent");

            try {
                choix = sc.nextInt();
            } catch ( InputMismatchException e ) {
                choix = -1;
            } finally {
                sc.nextLine();
            }
            first = false;
        } while ( choix < 1 || choix > 2 );

        switch (choix){
            case 1:
                do{
                    System.out.print("Montant à ajouter : ");
                    try {
                        montant = sc.nextDouble();
                    } catch ( InputMismatchException e ) {
                        montant = -1;
                    } finally {
                        sc.nextLine();
                    }
                }while (montant < 0);

                compte.versement(montant);

                try{
                    dao.update(compte);
                    System.out.println("Retrait effectué avec succès");
                    sc.nextLine();
                }catch ( IOException | ClassNotFoundException | SQLException e ) {
                    System.err.println( e.getMessage() );
                }
                break;

            case 2:
                do{
                    System.out.print("Montant à retirer : ");
                    try {
                        montant = sc.nextDouble();
                    } catch ( InputMismatchException e ) {
                        montant = -1;
                    } finally {
                        sc.nextLine();
                    }
                }while (montant < 0);

                compte.retrait(montant);

                try {
                    dao.update(compte);
                    System.out.println("Retrait effectué avec succès");
                    sc.nextLine();
                }catch ( IOException | ClassNotFoundException | SQLException e ) {
                    System.err.println( e.getMessage() );
                }
                break;
        }

        writeToCSV(compte, soldeAvantAction);

        dspMainMenu();
    }

    private static void supprimerCompte() {
        System.out.println( "======================================" );
        System.out.println( "======== SUPPRIMER UN COMPTE =========" );
        System.out.println( "======================================" );

        for (Compte compte : agence.getComptes()){
            System.out.printf("%s : %s", compte.getId(), compte);
        }

        Compte compte = null;
        int compteID;

        do{
            System.out.print("Choisissez le compte à supprimer :");

            try {
                compteID = sc.nextInt();
            }catch (InputMismatchException e){
                compteID = -1;
                System.out.println("Erreur de saisie, recommencez");
            } finally {
                sc.nextLine();
            }

            try{
                compte = dao.findById(compteID);
            }catch ( IOException | ClassNotFoundException | SQLException e ) {
                System.err.println( e.getMessage() );
            }
        }while (compte == null || compteID < 0);

        try{
            dao.remove(compte);
            System.out.println("Compte supprimé avec succès");
            sc.nextLine();
        }catch ( IOException | ClassNotFoundException | SQLException e ) {
            System.err.println( e.getMessage() );
        }

        dspMainMenu();
    }

    private static void listeComptes() {
        System.out.println( "======================================" );
        System.out.println( "========== LISTE DES COMPTES =========" );
        System.out.println( "======================================" );

        for (Compte compte : agence.getComptes()){
            System.out.printf("- %s %n", compte);
        }

        sc.nextLine();
        dspMainMenu();
    }

    public static void writeToCSV(Compte compte, double ancienSolde) {

        String bkpFileName = "compte_" + compte.getId() + ".csv";
        Path file = Paths.get( BACKUPS_DIR + bkpFileName );

        try (BufferedReader br = new BufferedReader( Files.newBufferedReader( file ) );
             BufferedWriter bw = new BufferedWriter( Files.newBufferedWriter( file ) ) ) {

            String line;

            while ( (line = br.readLine() ) != null ) {
                bw.write( line );
                bw.newLine();
            }

            final StringBuilder sb = new StringBuilder();
            sb.append(new SimpleDateFormat("dd/MM/yyyy").format(new Date())).append( ";" );
            sb.append(compte.getId() ).append(";");
            sb.append(ancienSolde).append(";");
            sb.append(compte.getSolde()).append(";");

            bw.write(sb.toString());
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    public static void createCSV(Compte compte) {

        Path bkpPath = Paths.get( BACKUPS_DIR );

        if ( !Files.isDirectory( bkpPath ) ) {
            try {
                Files.createDirectory( bkpPath );
            } catch ( IOException e ) {
                e.printStackTrace();
            }
        }

        String bkpFileName = "compte_" + compte.getId() + ".csv";

        Path file = Paths.get( BACKUPS_DIR + bkpFileName );

        try ( ObjectOutputStream oos = new ObjectOutputStream( Files.newOutputStream( file ) ) ) {
            oos.writeChars("Date;identifiant Compte; solde avant action; solde après action");
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }
}
