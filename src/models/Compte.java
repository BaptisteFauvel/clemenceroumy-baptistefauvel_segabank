package models;

public abstract class Compte {
    protected int id;
    protected double solde;
    protected Agence agence;

    public Agence getAgence() {
        return agence;
    }

    public void setAgence(Agence agence) {
        this.agence = agence;
    }

    public Compte(int id, double solde, Agence agence){
        this.id = id;
        this.solde = solde;
        this.agence = agence;
    }

    public void versement(double amount){
        if (amount > 0){
            this.solde += amount;
        }
        else {
            System.out.println("Le montant ne peut pas être négatif");
        }
    }
    public abstract void retrait(double amount);

    @Override
    public String toString(){
        final StringBuilder sb = new StringBuilder( "Compte Epargne : \n" );
        sb.append( " Compte n° : " ).append( id ).append("\n");
        sb.append( " Solde : " ).append(solde).append("\n");
        return sb.toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }
}
