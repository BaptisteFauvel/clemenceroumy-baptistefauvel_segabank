package models;

public class CompteSimple extends Compte {

    private double decouvert;

    public CompteSimple(int id, double solde, double decouvert, Agence agence){
        super(id, solde, agence);
        this.decouvert = decouvert;
    }

    @Override
    public void retrait(double amount) {
        if (this.solde + this.decouvert - amount >= 0){
            this.solde -= amount;
        }
        else {
            System.out.println("Solde insuffisant");
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder( "Compte Simple : \n" );
        sb.append( " Compte n° : " ).append( id ).append("\n");
        sb.append( " Solde : " ).append(solde).append("\n");
        sb.append( " Découvert autorisé : " ).append(decouvert).append("\n");
        return sb.toString();
    }

    public double getDecouvert() {
        return decouvert;
    }

    public void setDecouvert(double decouvert) {
        this.decouvert = decouvert;
    }
}
