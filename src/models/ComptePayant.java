package models;

public class ComptePayant extends Compte {

    private static final double TAXE = 0.05;

    public ComptePayant(int id, double solde, Agence agence){
        super(id, solde, agence);
    }

    @Override
    public void versement(double amount) {
        amount = amount - amount* TAXE;
        if(amount > 0){
            this.solde += amount;
        }
        else{
            System.out.println("Solde insuffisant");
        }
    }

    @Override
    public void retrait(double amount) {
        amount = amount + amount* TAXE;
        if (this.solde - amount >= 0){
            this.solde -= amount;
        }
        else {
            System.out.println("Solde insuffisant");
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder( "Compte Payant : \n" );
        sb.append( " Compte n° : " ).append( id ).append("\n");
        sb.append( " Solde : " ).append(solde).append("\n");
        return sb.toString();
    }
}
