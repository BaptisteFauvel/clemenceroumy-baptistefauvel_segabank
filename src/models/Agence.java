package models;

import dao.CompteDAO;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Agence {
    private int id;
    private String code;
    private String adresse;
    private List<Compte> comptes;

    public Agence(int id, String code, String adresse){
        this.id = id;
        this.code = code;
        this.adresse = adresse;
        this.comptes = new ArrayList<Compte>();
    }

    public Agence(int id, String code, String adresse, ArrayList<Compte> comptes) {
        this.id = id;
        this.code = code;
        this.adresse = adresse;
        this.comptes = comptes;
    }

    public Agence() {
        this.comptes = new ArrayList<Compte>();
    }

    public Compte getCompte(int idCompte){
        Compte compteSelectionne = null;

        for(Compte compte : this.comptes){
            if(compte.getId() == idCompte){
                compteSelectionne = compte;
            }
        }

        return compteSelectionne;
    }

    public CompteEpargne creerCompteEpargne (double solde, float taux){
        CompteEpargne compte = new CompteEpargne(1 ,solde, taux, this);
        try {
            new CompteDAO().create(compte);
        } catch (SQLException | IOException | ClassNotFoundException e) {
            System.err.println("Impossible de créer le compte");
        }
        this.ajouterCompte(compte);
        return compte;
    }

    public CompteSimple creerCompteSimple (double solde, double decouvert){
        CompteSimple compte = new CompteSimple(1 ,solde, decouvert, this);
        try {
            new CompteDAO().create(compte);
        } catch (SQLException | IOException | ClassNotFoundException e) {
            System.err.println("Impossible de créer le compte");
        }
        this.ajouterCompte(compte);
        return compte;
    }

    public ComptePayant creerComptePayant (double solde){
        ComptePayant compte = new ComptePayant(1 ,solde, this);
        try {
            new CompteDAO().create(compte);
        } catch (SQLException | IOException | ClassNotFoundException e) {
            System.err.println("Impossible de créer le compte");
        }
        this.ajouterCompte(compte);
        return compte;
    }

    private void ajouterCompte(Compte compte){
        this.comptes.add(compte);
    }

    public void modifierCompte(){

    }

    public void supprimerCompte(Compte compte){
        try {
            new CompteDAO().remove(compte);
            this.comptes.remove(compte);
        } catch (SQLException | IOException | ClassNotFoundException e) {
            System.err.println("Impossible de supprimer le compte");
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder( "AGENCE : \n" );
        sb.append( " Agence n° : " ).append( id ).append("\n");
        sb.append( " Code : " ).append(code).append("\n");
        sb.append( " Addresse : " ).append(adresse).append("\n");
        return sb.toString();
    }

    //region GETTERS/SETTERS
    //----------------------------------

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public List<Compte> getComptes() {
        return comptes;
    }

    public void setComptes(List<Compte> comptes) {
        this.comptes = comptes;
    }

    //endregion
}
