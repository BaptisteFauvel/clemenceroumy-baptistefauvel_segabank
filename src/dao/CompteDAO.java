package dao;

import models.Compte;
import models.CompteEpargne;
import models.ComptePayant;
import models.CompteSimple;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CompteDAO implements IDAO<Integer, Compte> {

    private static final String INSERT_QUERY = "INSERT INTO compte (solde, decouvert, tauxInteret, idAGENCE) VALUES(?,?,?,?)";
    private static final String UPDATE_QUERY = "UPDATE compte SET solde = ? WHERE idCOMPTE = ?";
    private static final String REMOVE_QUERY = "DELETE FROM compte WHERE idCOMPTE = ?";
    private static final String FIND_QUERY = "SELECT * FROM compte WHERE idCOMPTE = ?";
    private static final String FIND_ALL_QUERY = "SELECT * FROM compte";
    private static final String FIND_BY_AGENCY_QUERY = "SELECT * FROM compte WHERE idAGENCE = ?";

    @Override
    public void create(Compte compte) throws SQLException, IOException, ClassNotFoundException {

        Connection connection = PersistenceManager.getConnection();

        if (connection != null) {
            try (PreparedStatement ps = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {
                ps.setDouble(1, compte.getSolde());
                if (compte instanceof CompteSimple) {
                    ps.setDouble(2, ((CompteSimple) compte).getDecouvert());
                    ps.setDouble(3, 0);
                } else if (compte instanceof CompteEpargne) {
                    ps.setDouble(2, 0);
                    ps.setDouble(3, ((CompteEpargne) compte).getTauxInteret());
                } else {
                    ps.setDouble(2, 0);
                    ps.setDouble(3, 0);
                }

                ps.setInt(4, compte.getAgence().getId());

                ps.executeUpdate();
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs.next()) {
                        compte.setId(rs.getInt(1));
                    }
                }
            }
        }
    }

    @Override
    public void update(Compte compte) throws SQLException, IOException, ClassNotFoundException {

        Connection connection = PersistenceManager.getConnection();

        if (connection != null) {
            try (PreparedStatement ps = connection.prepareStatement(UPDATE_QUERY)) {
                ps.setDouble(1, compte.getSolde());
                ps.setInt(2, compte.getId());
                ps.executeUpdate();
            }
        }
    }

    @Override
    public void remove(Compte compte) throws SQLException, IOException, ClassNotFoundException {

        Connection connection = PersistenceManager.getConnection();

        if (connection != null) {
            try (PreparedStatement ps = connection.prepareStatement(REMOVE_QUERY)) {
                ps.setInt(1, compte.getId());
                ps.executeUpdate();
            }
        }
    }

    @Override
    public Compte findById(Integer compteID) throws SQLException, IOException, ClassNotFoundException {
        Compte compte = null;
        Connection connection = PersistenceManager.getConnection();
        if (connection != null) {
            try (PreparedStatement ps = connection.prepareStatement(FIND_QUERY)) {
                ps.setInt(1, compteID);
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        if (rs.getDouble("decouvert") != (double) 0) {
                            compte = new CompteSimple(rs.getInt("idCOMPTE"), rs.getDouble("solde"), rs.getDouble("decouvert"), new AgenceDAO().findById(rs.getInt("idAGENCE")));
                        } else if (rs.getDouble("tauxInteret") != (double) 0) {
                            compte = new CompteEpargne(rs.getInt("idCOMPTE"), rs.getDouble("solde"), rs.getDouble("tauxInteret"), new AgenceDAO().findById(rs.getInt("idAGENCE")));
                        } else {
                            compte = new ComptePayant(rs.getInt("idCOMPTE"), rs.getDouble("solde"), new AgenceDAO().findById(rs.getInt("idAGENCE")));
                        }
                    }
                }
            }
        }
        return compte;
    }

    @Override
    public List<Compte> findAll() throws SQLException, IOException, ClassNotFoundException {

        List<Compte> list = new ArrayList<>();
        Connection connection = PersistenceManager.getConnection();

        if (connection != null) {
            try (PreparedStatement ps = connection.prepareStatement(FIND_ALL_QUERY)) {
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        if (rs.getDouble("decouvert") != (double) 0) {
                            list.add(new CompteSimple(rs.getInt("idCOMPTE"), rs.getDouble("solde"), rs.getDouble("decouvert"), new AgenceDAO().findById(rs.getInt("idAGENCE"))));
                        } else if (rs.getDouble("tauxInteret") != (double) 0) {
                            list.add(new CompteEpargne(rs.getInt("idCOMPTE"), rs.getDouble("solde"), rs.getDouble("tauxInteret"), new AgenceDAO().findById(rs.getInt("idAGENCE"))));
                        } else {
                            list.add(new ComptePayant(rs.getInt("idCOMPTE"), rs.getDouble("solde"), new AgenceDAO().findById(rs.getInt("idAGENCE"))));
                        }
                    }
                }
            }
        }
        return list;
    }

    @Override
    public List<Compte> findByAgence(int agence) throws SQLException, IOException, ClassNotFoundException {

        List<Compte> list = new ArrayList<>();
        Connection connection = PersistenceManager.getConnection();

        if (connection != null) {
            try (PreparedStatement ps = connection.prepareStatement(FIND_BY_AGENCY_QUERY)) {
                ps.setInt(1, agence);
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        if (rs.getDouble("decouvert") != (double) 0) {
                            list.add(new CompteSimple(rs.getInt("idCOMPTE"), rs.getDouble("solde"), rs.getDouble("decouvert"), new AgenceDAO().findById(rs.getInt("idAGENCE"))));
                        } else if (rs.getDouble("tauxInteret") != (double) 0) {
                            list.add(new CompteEpargne(rs.getInt("idCOMPTE"), rs.getDouble("solde"), rs.getDouble("tauxInteret"), new AgenceDAO().findById(rs.getInt("idAGENCE"))));
                        } else {
                            list.add(new ComptePayant(rs.getInt("idCOMPTE"), rs.getDouble("solde"), new AgenceDAO().findById(rs.getInt("idAGENCE"))));
                        }
                    }
                }
            }
        }
        return list;
    }

    @Override
    public Compte findByCode(String code) throws SQLException, IOException, ClassNotFoundException {
        return null;
    }
}

