package dao;

import models.Agence;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AgenceDAO implements IDAO<Integer, Agence> {

    private static final String INSERT_QUERY = "INSERT INTO agence (code, adresse) VALUES(?,?)";
    private static final String UPDATE_QUERY = "UPDATE agence SET code = ?, adresse = ? WHERE idAGENCE = ?";
    private static final String REMOVE_QUERY = "DELETE FROM agence WHERE idAGENCE = ?";
    private static final String FIND_QUERY = "SELECT * FROM agence WHERE idAGENCE = ?";
    private static final String FIND_BY_CODE_QUERY = "SELECT * FROM agence WHERE code = ?";
    private static final String FIND_ALL_QUERY = "SELECT * FROM agence";

    @Override
    public void create(Agence agence) throws SQLException, IOException, ClassNotFoundException {
        Connection connection = PersistenceManager.getConnection();
        if ( connection != null ) {
            try ( PreparedStatement ps = connection
                    .prepareStatement( INSERT_QUERY, Statement.RETURN_GENERATED_KEYS ) ) {
                ps.setString( 1, agence.getCode() );
                ps.setString( 2, agence.getAdresse());
                ps.executeUpdate();
                try ( ResultSet rs = ps.getGeneratedKeys() ) {
                    if ( rs.next() ) {
                        agence.setId( rs.getInt( 1 ) );
                    }
                }
            }
        }
    }

    @Override
    public void update(Agence agence) throws SQLException, IOException, ClassNotFoundException {
        Connection connection = PersistenceManager.getConnection();
        if ( connection != null ) {
            try ( PreparedStatement ps = connection.prepareStatement( UPDATE_QUERY ) ) {
                ps.setString( 1, agence.getCode() );
                ps.setString( 3, agence.getAdresse() );
                ps.executeUpdate();
            }
        }
    }

    @Override
    public void remove(Agence agence) throws SQLException, IOException, ClassNotFoundException {
        Connection connection = PersistenceManager.getConnection();
        if ( connection != null ) {
            try ( PreparedStatement ps = connection.prepareStatement( REMOVE_QUERY ) ) {
                ps.setLong( 1, agence.getId() );
                ps.executeUpdate();
            }
        }
    }

    @Override
    public Agence findById(Integer id) throws SQLException, IOException, ClassNotFoundException {
        Agence agence = null;
        Connection connection = PersistenceManager.getConnection();
        if ( connection != null ) {
            try ( PreparedStatement ps = connection.prepareStatement( FIND_QUERY ) ) {
                ps.setInt( 1, id );
                try ( ResultSet rs = ps.executeQuery() ) {
                    if ( rs.next() ) {
                        agence = new Agence();
                        agence.setId( rs.getInt( 1 ) );
                        agence.setCode( rs.getString( 2 ) );
                        agence.setAdresse( rs.getString( 3 ) );
                    }
                }
            }
        }
        return agence;
    }

    @Override
    public List<Agence> findAll() throws SQLException, IOException, ClassNotFoundException {
        List<Agence> list = new ArrayList<>();
        Connection connection = PersistenceManager.getConnection();
        if ( connection != null ) {
            try ( PreparedStatement ps = connection.prepareStatement( FIND_ALL_QUERY ) ) {
                try ( ResultSet rs = ps.executeQuery() ) {
                    while ( rs.next() ) {
                        Agence agence = new Agence();
                        agence.setId( rs.getInt( 1 ) );
                        agence.setCode( rs.getString( 2 ) );
                        agence.setAdresse( rs.getString( 3 ) );
                        list.add( agence );
                    }
                }
            }
        }
        return list;
    }

    @Override
    public List<Agence> findByAgence(int agence) {
        return null;
    }

    @Override
    public Agence findByCode(String code) throws SQLException, IOException, ClassNotFoundException {
        Agence agence = null;
        Connection connection = PersistenceManager.getConnection();
        if ( connection != null ) {
            try ( PreparedStatement ps = connection.prepareStatement( FIND_BY_CODE_QUERY ) ) {
                ps.setString( 1, code );
                try ( ResultSet rs = ps.executeQuery() ) {
                    if ( rs.next() ) {
                        agence = new Agence();
                        agence.setId( rs.getInt( 1 ) );
                        agence.setCode( rs.getString( 2 ) );
                        agence.setAdresse( rs.getString( 3 ) );
                    }
                }
            }
        }
        return agence;
    }
}
