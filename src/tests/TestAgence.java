package tests;

import dao.AgenceDAO;
import dao.CompteDAO;
import models.*;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

public class TestAgence {
    public static void main(String[] args) {
        Agence agence = new Agence(1, "NTE44100", "rue de la banque");
        try {
            new AgenceDAO().create(agence);
        } catch (SQLIntegrityConstraintViolationException e){
            System.out.println("L'agence existe déjà en base");
            try {
                agence = new AgenceDAO().findByCode(agence.getCode());
                System.out.println("Agence récupérée avec succès");
            } catch (SQLException | IOException | ClassNotFoundException ex) {
                System.err.println("Impossible de récupérer l'agence");
            }

            try {
                agence.setComptes(new CompteDAO().findByAgence(agence.getId()));
            } catch (SQLException | IOException | ClassNotFoundException exe) {
                System.out.println("Impossible de récupérer les comptes");
            }
        }
        catch (SQLException | IOException | ClassNotFoundException e) {
            System.err.println("Impossible de créer l'agence en base");
        }

        CompteEpargne compteEpargne = agence.creerCompteEpargne(300, (float) 0.5);
        CompteSimple compteSimple = agence.creerCompteSimple(300, 100);
        ComptePayant comptePayant = agence.creerComptePayant(300);

        System.out.println(agence.getCompte(2));

        System.out.println("Test Compte Epargne");
        System.out.println(compteEpargne);
        System.out.println("Retrait de 300€");
        compteEpargne.retrait(300);
        System.out.println(compteEpargne.getSolde());

        System.out.println("Retrait de 550€");
        compteEpargne.retrait(550);
        System.out.printf("Solde : %f€ %n",compteEpargne.getSolde());

        System.out.println("Ajout de 100€");
        compteEpargne.versement(100);
        System.out.printf("Solde : %f€ %n", compteEpargne.getSolde());

        System.out.println();
        System.out.printf("Estimation après intérets : %f€ %n", compteEpargne.estimationInterets());

        System.out.println("Calculs intérets");
        compteEpargne.calculInterets();
        System.out.printf("Solde : %f€ %n",compteEpargne.getSolde());

        System.out.println("//////////////////////");

        System.out.println("Test Compte Payant");
        System.out.println(comptePayant);
        System.out.println("Retrait de 300€");
        comptePayant.retrait(300);
        System.out.printf("Solde : %f€ %n",comptePayant.getSolde());

        System.out.println("Retrait de 550€");
        comptePayant.retrait(550);
        System.out.printf("Solde : %f€ %n",comptePayant.getSolde());

        System.out.println("Ajout de 100€");
        comptePayant.versement(100);
        System.out.printf("Solde : %f€ %n",comptePayant.getSolde());

        System.out.println("//////////////////////");

        System.out.println("Test Compte Simple");
        System.out.println(compteSimple);
        System.out.println("Retrait de 400€");
        compteSimple.retrait(400);
        System.out.printf("Solde : %f€ %n",compteSimple.getSolde());

        System.out.println("Retrait de 750€");
        compteSimple.retrait(750.5);
        System.out.printf("Solde : %f€ %n",compteSimple.getSolde());

        System.out.println("Ajout de 150");
        compteSimple.versement(150);
        System.out.printf("Solde : %f€ %n",compteSimple.getSolde());

        System.out.println("Ajout de -2€");
        compteSimple.versement(-2);
        System.out.printf("Solde : %f€ %n",compteSimple.getSolde());
    }
}
