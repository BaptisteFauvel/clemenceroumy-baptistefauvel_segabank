create schema if not exists segabank collate latin1_swedish_ci;

create table if not exists agence
(
    idAGENCE int auto_increment
        primary key,
    code varchar(45) null,
    adresse varchar(45) null,
    constraint Agence_code_uindex
        unique (code)
);

create table if not exists compte
(
    idCOMPTE int auto_increment
        primary key,
    solde double null,
    decouvert double null,
    tauxInteret float null,
    idAGENCE int null
);

create index compte_agence_idAGENCE_fk
    on compte (idAGENCE);

